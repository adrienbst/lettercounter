﻿using System;
using Metier.Interfaces;
using Metier.Entities;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Metier
{
    public class Controller : IObservable
    {
        private List<IObserver> _observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            this._observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            this._observers.Remove(observer);
        }

        //Cette fonction sert à mettre à jour notre barre de progression
        public void Notify()
        {
            foreach (var observer in _observers)
            {
                observer.Update(this, operationsAmount);
            }
        }

        private Ivue view;

        public void Setview(Ivue view)
        {
            this.view = view;
        }

        private Resultat results = new Resultat();
        private int operationsAmount = 0;

        //Fonction principale du controller, c'est ici que tout le traitement est géré.
        public void GetStats(string fileName, List<string> selectedLetters)
        {
            Thread t = new Thread(() =>
            {
                List<string> words = SplitWords(fileName);

                //Pour calculer la taille maximum de notre progress bar, on regarde la taille des listes à traiter
                operationsAmount = words.Count + selectedLetters.Count;

                view.displayStats(CountLetters(fileName, selectedLetters), CountWords(words));
            });

            t.Start();
        }

        //On l'explose le fichier dans une liste de string
        private List<string> SplitWords(string fileName)
        {
            //TODO: Ouvrir le fichier une seule fois
            string fileContent = File.ReadAllText(fileName);

            //Ce tableau de caractères sert à lister les caractères "délimitants"
            char[] delimiterChars = { ' ', '\n' };

            List<string> result = new List<string>(fileContent.Split(delimiterChars));

            return result;
        }

        //On range nos mots dans une liste de <WordLengths>.
        //Chaque élément de la liste contient une taille, et le nombre de mots qui y ont été rangés
        public List<WordLengths> CountWords(List<string> words)
        {
            List<WordLengths> wordLengths = new List<WordLengths>();

            foreach (var word in words)
            {
                //On vérifie que la longueur du mot est différente de 0
                //Puis on regarde si pour tout "x" de "wordLengths" il n'existe pas une longueur équivalente à "word.Length"

                //En gros si aucun mot de même longueur n'as déjà été compté, on ajoute un élément à la liste.
                if (word.Length != 0 && wordLengths.Exists(x => x.length == word.Length) == false)
                {
                    wordLengths.Add(new WordLengths(word.Length, 1));
                }
                //Si un mot de même longueur a déjà été compté, on incrémente son compteur
                else if (word.Length != 0 && wordLengths.Exists(x => x.length == word.Length) == true)
                {
                    wordLengths.Find(x => x.length.Equals(word.Length)).count++;
                }

                Notify();
            }

            //TODO: utiliser l'objet results pour tout faire
            results.wordLengths = wordLengths;

            //On tri notre liste pour qu'elle soit plus ordonnée une fois affichée dans la vue
            //Pour ça, on utilise la lib "linq" : https://docs.microsoft.com/fr-fr/dotnet/api/system.linq?view=net-5.0
            List<WordLengths> orderedWordLengths = wordLengths.OrderBy(n => n.length).ToList();

            return orderedWordLengths;
        }

        //Compte la fréquence d'apparition des lettres sélectionnées par l'utilisateur
        //Puis retourne une liste de string
        public List<string> CountLetters(string fileName, List<string> letters)
        {
            //TODO: Ouvrir le fichier une seule fois
            string text = System.IO.File.ReadAllText(fileName);
            int count;
            List<string> result = new List<string>();

            foreach (var letter in letters)
            {
                //On remplace la lettre qu'on veut compter par "rien"
                //Puis, on compare la différence de longueur entre les deux chaine.
                count = text.Length - text.Replace(letter, "").Length;

                result.Add("Le nombre de " + letter + " est de : " + count.ToString() + "\n");

                Notify();
            }

            //TODO utiliser l'objet result pour tout faire
            results.letterFrequencies = result;

            return result;
        }

        //Prend l'objet result -> le converti au format JSON dans un string -> Enregistre le fichier
        public void saveJSON(string fileName)
        {
            string output = JsonConvert.SerializeObject(results);

            File.WriteAllText(fileName, output);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Metier.Entities;

namespace Metier.Interfaces
{
    public interface Ivue
    {

        public void displayStats(List<string> letterCount, List<WordLengths> wordLenghts);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metier.Interfaces
{
    public interface IObservable
    {
        //attach an observer to the subject
        void Attach(IObserver observer);

        //detach an observer from the subject
        void Detach(IObserver observer);

        //notify all observers about an event
        void Notify();
    }
}

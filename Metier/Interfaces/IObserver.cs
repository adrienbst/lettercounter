﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metier.Interfaces
{
    public interface IObserver
    {
        //Receive updates from observable
        public void Update(IObservable observable, int operationsAmount);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metier.Entities
{
    public class WordLengths
    {
        public WordLengths(int length, int count)
        {
            this.length = length;
            this.count = count;
        }

        public int length;
        public int count;
    }
}

﻿using Metier;
using Metier.Entities;
using Metier.Interfaces;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Windows;

namespace vue
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, Ivue, IObserver
    {
        Controller c;

        //Créer une liste de checkbox
        private List<System.Windows.Controls.CheckBox> checkBoxes = new List<System.Windows.Controls.CheckBox>();

        //Ajouter toutes les checkboxes à la liste
        private void GetCheckboxes()
        {
            checkBoxes.Add(cb_a);
            checkBoxes.Add(cb_b);
            checkBoxes.Add(cb_c);
            checkBoxes.Add(cb_d);
            checkBoxes.Add(cb_e);
            checkBoxes.Add(cb_f);
            checkBoxes.Add(cb_g);
            checkBoxes.Add(cb_h);
            checkBoxes.Add(cb_i);
            checkBoxes.Add(cb_j);
            checkBoxes.Add(cb_k);
            checkBoxes.Add(cb_l);
            checkBoxes.Add(cb_m);
            checkBoxes.Add(cb_n);
            checkBoxes.Add(cb_o);
            checkBoxes.Add(cb_p);
            checkBoxes.Add(cb_q);
            checkBoxes.Add(cb_r);
            checkBoxes.Add(cb_s);
            checkBoxes.Add(cb_t);
            checkBoxes.Add(cb_u);
            checkBoxes.Add(cb_v);
            checkBoxes.Add(cb_w);
            checkBoxes.Add(cb_x);
            checkBoxes.Add(cb_y);
            checkBoxes.Add(cb_z);
        }
        public MainWindow()
        {
            InitializeComponent();
            c = new Controller();
            c.Setview(this);
            c.Attach(this);
            GetCheckboxes();
        }
 
        int currentOperation = 0;

        //Cette fonction est appelée pour actualiser la progress bar
        public void Update(IObservable subject, int operationsAmount)
        {
            currentOperation++;

            //CF ligne 75
            Dispatcher.Invoke(delegate ()
            {
                ProgressBar.Value = ((currentOperation * 100) / operationsAmount);
            });
        }

        //Cette fonction est appelée pour afficher les résultats une le traitement terminé
        public void displayStats(List<string> letterCount, List<WordLengths> wordLenghts)
        {

            //Un thread d'arrière plan ne peut pas mettre à jour le contenu d’un de qqch qui a été créé sur le thread d’interface utilisateur.
            //Pour pouvoir le faire , le thread d’arrière-plan doit déléguer le travail au Dispatcher: https://docs.microsoft.com/fr-fr/dotnet/api/system.windows.threading.dispatcher.invoke?view=net-5.0

            Dispatcher.Invoke(delegate ()
            {
                string words = "";
                foreach (var length in wordLenghts)
                {
                    words += "Le nombre de mots longs de " + length.length + " lettres est de " + length.count + "\n";
                }

                string letters = "";
                foreach (var count in letterCount)
                {
                    letters = letters + count;
                }

                txt_word.Text = words;
                txt_letter.Text = letters;
            });
        }

        //Récupère le clique du bouton "browse"
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            //Ici je créer une class de Win32 "OpenFileDialog", qui me permet d'utiliser l'explorateur de fichiers windows
            OpenFileDialog openFileDialog = new OpenFileDialog();

            //La méthode "ShowDialog", elle me permet d'ouvrir l'explorateur de fichiers
            if (openFileDialog.ShowDialog() == true)
            {

                List<string> lettersToCount = GetLetters();

                c.GetStats(openFileDialog.FileName, lettersToCount);

                btn_savejson.IsEnabled = true;
            } 
        }

        //Récupère le clique du bouton "save"
        public void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            //On ouvre une fenêtre "saveFileDialog" et on applique un filtre pour empêcher d'enregistrer à un autre format
            saveFileDialog.Filter = "Fichier JSON (*.json)|*.json";
            saveFileDialog.FilterIndex = 1;

            if (saveFileDialog.ShowDialog() == true)
            {
                c.saveJSON(saveFileDialog.FileName);
            }
            else
            {
                MessageBox.Show("Impossible d'enregistrer le fichier, l'emplacement choisi n'est pas valide ");
            }
        }

        //Fonction dégueu pour récupérer l'état des checkboxes
        //TODO: https://www.codesenior.com/en/tutorial/WPF-Create-Custom-Checkbox-List-With-ListBox-Element
        List<string> GetLetters()
        {
            List<string> letters = new List<string>();

            foreach (var checkbox in checkBoxes)
            {
                if (checkbox.IsChecked == true)
                {
                    string letter = checkbox.Name.Replace("cb_", "");
                    letters.Add(letter);
                }
            }
            return letters;
        }
    }
}
